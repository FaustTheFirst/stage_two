import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  const createProperty = propertyKeyValue => {
    const fighterProperty = createElement({ tagName: 'span', className: 'fighter-preview___property' });
    fighterProperty.innerText = propertyKeyValue.join(': ').replace(/(\w+):/, subStr => subStr.toUpperCase());
  
    return fighterProperty;
  }

  if(fighter) {
    const arrayOfAllFighterProperties = Object.entries(fighter);
    fighterElement.append(createFighterImage(fighter));
    arrayOfAllFighterProperties
      .filter(propertyKeyValue => propertyKeyValue[0] !== '_id' && propertyKeyValue[0] !== 'source')
      .forEach(propertyKeyValue => fighterElement.append(createProperty(propertyKeyValue)));
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
