import { controls } from '../../constants/controls';
import { createElement } from '../helpers/domHelper';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const healthIndicatorDiv = document.getElementsByClassName('arena___health-indicator');
    const transitionHandler = index => document.getElementById(index).remove();
    const fightProperties = {
      fighters: {0: {...firstFighter}, 1: {...secondFighter}},
      playersHealth: [100, 100],
      playersHealthBars: [
        document.getElementById('left-fighter-indicator'),
        document.getElementById('right-fighter-indicator')
      ],
      controlsInput: new Set(),
      lastCritAttack: [Date.now(), Date.now()],
      critCombinations: [controls.PlayerOneCriticalHitCombination, controls.PlayerTwoCriticalHitCombination]
    }
    
    document.addEventListener('keydown', e => {
      const updatedFightProperties = keyDownHandler(e, fightProperties);
      if (updatedFightProperties !== null) {
        if (updatedFightProperties.playersHealth[0] <= 0) resolve(secondFighter);
        else if (updatedFightProperties.playersHealth[1] <= 0) resolve(firstFighter);
        Object.assign(fightProperties, updatedFightProperties);
      }  
    });

    document.addEventListener('keyup', e => {
      const updatedFightProperties = keyUpHandler(e, fightProperties);
      if (updatedFightProperties !== null) {
        Object.assign(fightProperties, updatedFightProperties);
      }
    });

    healthIndicatorDiv[0].addEventListener('transitionend', () => transitionHandler(0));
    healthIndicatorDiv[1].addEventListener('transitionend', () => transitionHandler(1));
  });
}

export function getDamage(attacker, criticalStrike = false, defender) {
  const damage = getHitPower(attacker, criticalStrike) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter, criticalStrike = false) {
  const criticalHitChance = criticalStrike ? 2 : Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodjeChance = Math.random() + 1;
  return fighter.defense * dodjeChance;
}

// handles key down that matches controls
const keyDownHandler = (e, fightProperties) => {
  if(!e.repeat) {
    switch(e.code) {
      case controls.PlayerOneAttack: {
        fightProperties.controlsInput.add(controls.PlayerOneAttack);
        return applyAttack(fightProperties, 0, 1);
      }

      case controls.PlayerTwoAttack: {
        fightProperties.controlsInput.add(controls.PlayerTwoAttack);
        return applyAttack(fightProperties, 1, 0);
      }

      case controls.PlayerOneBlock: {
        fightProperties.controlsInput.add(controls.PlayerOneBlock);
        break;
      }

      case controls.PlayerTwoBlock: {
        fightProperties.controlsInput.add(controls.PlayerTwoBlock);
        break;
      }
    }

    if (controls.PlayerOneCriticalHitCombination.includes(e.code)) {
      fightProperties.controlsInput.add(e.code);
      return criticalAttack(fightProperties, 0, 1);
    }

    if (controls.PlayerTwoCriticalHitCombination.includes(e.code)) {
      fightProperties.controlsInput.add(e.code);
      return criticalAttack(fightProperties, 1, 0);
    }

    return fightProperties;
  }

  return null;
}

// handles key up
const keyUpHandler = (e, fightProperties) => {
  if (fightProperties.controlsInput.has(e.code)) {
    fightProperties.controlsInput.delete(e.code);
    return fightProperties;
  }

  return null;
}

// handles status info that shows below health bar
const showStatus = (text, position) => {
  const activeStatusMarker = document.getElementById(position);

  if (activeStatusMarker) {
    activeStatusMarker.remove();
  }

  const statusMarker = createElement({ tagName: 'div', className: 'arena___status-marker', attributes: { id: position } });  

  statusMarker.innerText = text;

  document.getElementsByClassName('arena___health-indicator')[position]
    .appendChild(statusMarker).focus();
  document.getElementById(position)
  .classList.replace('arena___status-marker', 'arena___status-marker-after');
}

// handles damage dealing
const applyAttack = (fightProperties, attacker, defender, criticalStrike = false) => {
  if (criticalStrike) {
    showStatus('crit!', attacker);
  } else {
    if (fightProperties.controlsInput.has(controls.PlayerOneAttack)
      && fightProperties.controlsInput.has(controls.PlayerOneBlock)
      || fightProperties.controlsInput.has(controls.PlayerTwoAttack)
      && fightProperties.controlsInput.has(controls.PlayerTwoBlock)) {
      showStatus('Cannot attack', attacker);
      return null;
    }

    if (fightProperties.controlsInput.has(controls.PlayerOneBlock)
      || fightProperties.controlsInput.has(controls.PlayerTwoBlock)) {
      showStatus('block', defender);
      return null;
    }
  }

  const damageDealt = getDamage(fightProperties.fighters[attacker],
                                criticalStrike,
                                fightProperties.fighters[defender]);

  if (!damageDealt) {
    showStatus('miss', attacker);
    return null;
  }

  showStatus(`-${damageDealt.toFixed(1)}`, defender);
  fightProperties.playersHealth[defender] = fightProperties.playersHealth[defender] - damageDealt / fightProperties.fighters[defender].health * 100;
  fightProperties.playersHealthBars[defender].style.width = `${fightProperties.playersHealth[defender]}%`;

  return fightProperties;
}

// handles critical hit realization
const criticalAttack = (fightProperties, attacker, defender) => {
  const currentTime = Date.now();

  for (let key of fightProperties.critCombinations[attacker]) {
    if (!fightProperties.controlsInput.has(key)) {
      return null;
    }
  }

  if (currentTime - fightProperties.lastCritAttack[attacker] < 10000) {
    showStatus('not ready', attacker);
    return null;
  }

  fightProperties.lastCritAttack[attacker] = currentTime;

  return applyAttack(fightProperties, attacker, defender, true);
}
